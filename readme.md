Example for adding slurm capabilities inside an ubuntu 18.04 LTS singularity container.

from a plain ubuntu 18.04 LTS singularity container:
```
Singularity> ldd /pasteur/sonic/hpc/slurm/maestro/slurm/bin/scontrol
        linux-vdso.so.1 (0x00007ffd10de1000)
        libslurmfull.so => /pasteur/sonic/hpc/slurm/maestro/slurm_install/slurm-20.02/lib/slurm/libslurmfull.so (0x00007f4ab292b000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f4ab2923000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f4ab27d4000)
        libreadline.so.7 => not found
        libhistory.so.7 => not found
        libresolv.so.2 => /lib/x86_64-linux-gnu/libresolv.so.2 (0x00007f4ab27b8000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f4ab2793000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f4ab25a1000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f4ab2d2b000)
```
Adding slurm executable PATH: /pasteur/sonic/hpc/slurm/maestro/slurm/bin

You need to create a password file containing the slurm user (matching the uid from your cluster).
Run with:
```
$ module add singularity 
$ singularity shell -B /opt/gensoft -B /etc/munge -B /var/run/munge -B /pasteur -B ~/slurm.d/passwd-slurm:/etc/passwd  ~/singularity.d/containers/ubuntu-lts18.04-maestro-2021-05-07-1303.sif
```
more details at https://confluence.pasteur.fr/display/~tru/2021/05/18/slurm+from+within+a+singularity+container
